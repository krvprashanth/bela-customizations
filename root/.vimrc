source /usr/share/vim/vim82/defaults.vim
source /usr/share/vim/vim82/syntax/syntax.vim
source /usr/share/vim/vim82/syntax/synload.vim
source /usr/share/vim/vim82/syntax/syncolor.vim
source /usr/share/vim/vim82/filetype.vim
source /usr/share/vim/vim82/ftplugin.vim
source /usr/share/vim/vim82/indent.vim
set mouse=
color desert

